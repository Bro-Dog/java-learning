package com.brodog.juc.atomic;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/**
 * 让某一个对象的某一个属性实现原子类操作
 * @author By-BroDog
 * @date 2024-05-20
 */
public class AtomicReferenceFieldUpdaterDemo {

    public static void main(String[] args) {
        AtomicIntegerFieldUpdater<User>  updater = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");
        User user = new User("张三", 20);
        System.out.println("原子类修改 自增: " + updater.getAndIncrement(user));
        System.out.println("原子类修改 手动新增：: " + updater.getAndAdd(user, 30));
        System.out.println("最终值: " + updater.get(user));
    }

    public static class User {
        private String name;
        // 注意：要做原子操作的类属性必须是 volatile 修饰的
        public volatile int age;
        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
