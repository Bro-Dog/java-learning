package com.brodog.juc.atomic;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * 使用AtomicStampedReference类的印戳功能可是解决 ABA 问题，与使用 version 版本号的方式雷同
 * @author By-BroDog
 * @date 2024-05-20
 */
public class AtomicStampedReferenceDemo {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);
        AtomicStampedReference<Integer> atomicStampedReference = new AtomicStampedReference<>(1, 0);
        new Thread(() -> {
            boolean success = true;
            int stamp = atomicStampedReference.getStamp();
            System.out.println("线程一睡眠前：value= " + atomicStampedReference.getReference() + "  stamp = " + atomicStampedReference.getStamp());
            try {
                Thread.sleep(500L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            // 第一次修改
            success = atomicStampedReference.compareAndSet(1, 10, stamp, stamp + 1);
            System.out.println("线程一第一次修改 success: " + success + " value= " + atomicStampedReference.getReference() + " stamp = " + atomicStampedReference.getStamp());

            // 增加印戳，然后更新，如果印戳被别的线程修改了，则更新失败
            stamp++;
            success = atomicStampedReference.compareAndSet(10, 1, stamp, stamp+1);
            System.out.println("线程一第二次修改 success: " + success + " value= " + atomicStampedReference.getReference() + " stamp = " + atomicStampedReference.getStamp());
            latch.countDown();
        }).start();

        new Thread(() -> {
            boolean success = true;
            int stamp = atomicStampedReference.getStamp();
            System.out.println("线程二睡眠前：value= " + atomicStampedReference.getReference() + " stamp = " + atomicStampedReference.getStamp());
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("线程二睡眠后：value= " + atomicStampedReference.getReference() + " stamp = " + atomicStampedReference.getStamp());

            // 这个时候其实 stamp 已经被修改了，后续的 CAS 操作将会失败
            success = atomicStampedReference.compareAndSet(1, 20, stamp, stamp+1);
            System.out.println("线程二第一次修改 success: " + success + " value= " + atomicStampedReference.getReference() + " stamp = " + atomicStampedReference.getStamp());

            latch.countDown();
        }).start();

        latch.await();
    }
}
