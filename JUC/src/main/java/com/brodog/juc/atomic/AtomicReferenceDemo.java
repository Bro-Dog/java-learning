package com.brodog.juc.atomic;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 基本数据类型的原子操作类只能保证一个变量的原子操作，如果是对一个引用类型的变量的多个值进行修改，这就不行了，得使用原子引用类型来保证对象引用的原子性
 *
 * @author By-BroDog
 * @date 2024-05-20
 */
public class AtomicReferenceDemo {

    public static void main(String[] args) {
        // 包装的原子对象
        AtomicReference<User> userRef = new AtomicReference<>();

        User user = new User("张三", 18);
        userRef.set(user);

        User newUser = new User("李四", 20);

        // 使用 CAS 替换 user 对象的值
        boolean b = userRef.compareAndSet(user, newUser);
        System.out.println("CAS 结果：" + b);
        User user1 = userRef.get();
        System.out.println(user1);
    }

    public static class User {
        private String name;
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
