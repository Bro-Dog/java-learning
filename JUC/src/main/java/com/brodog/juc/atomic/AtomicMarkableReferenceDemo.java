package com.brodog.juc.atomic;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * AtomicMarkableReference 作为AtomicStampedReference的简化版，可以用来解决 ABA 问题
 * @author By-BroDog
 * @date 2024-05-20
 */
public class AtomicMarkableReferenceDemo {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);
        AtomicMarkableReference<Integer> atomicMarkableReference  = new AtomicMarkableReference<>(1, false);
        new Thread(() -> {
            boolean success = false;
            Integer value = atomicMarkableReference.getReference();
            boolean mark = getMark(atomicMarkableReference);
            System.out.println("线程一睡眠前：value= " + value + "  mark = " + mark);
            try {
                Thread.sleep(500L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            success = atomicMarkableReference.compareAndSet(1, 10, mark, !mark);
            System.out.println("线程一睡眠后：success = " + success +" value= " + atomicMarkableReference.getReference() + "  mark = " + getMark(atomicMarkableReference));
            latch.countDown();
        }).start();

        new Thread(() -> {
            boolean success = false;
            Integer value = atomicMarkableReference.getReference();
            boolean mark = getMark(atomicMarkableReference);
            System.out.println("线程二睡眠前：value= " + value + "  mark = " + mark);
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("线程二睡眠后: value= " + atomicMarkableReference.getReference() + "  mark = " + getMark(atomicMarkableReference));
            success = atomicMarkableReference.compareAndSet(1, 20, mark, !mark);
            System.out.println("线程二修改结果：success = " + success +" value= " + atomicMarkableReference.getReference() + "  mark = " + getMark(atomicMarkableReference));
            latch.countDown();
        }).start();

        latch.await();
    }

    private static boolean getMark(AtomicMarkableReference<Integer> atomicMarkableReference) {
        boolean[] markHolder = {false};
        int value = atomicMarkableReference.get(markHolder);
        System.out.println("value: " + value);
        return markHolder[0];
    }
}
