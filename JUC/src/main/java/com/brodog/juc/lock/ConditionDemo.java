package com.brodog.juc.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Condition 是 Lock 显示锁中实现的一套类似 Java 内置锁实现的“等待 - 通知”的一种线程间通信的模式；
 * 就像是 Object.wait()和 Object.notify()、Object.notifyAll();
 * @author By-BroDog
 * @date 2024-05-23
 */
public class ConditionDemo {
    static Lock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();

    public static void main(String[] args) {

        new Thread(() -> {
            lock.lock();

            try {
                System.out.println("准备开始等待...");
                condition.await();
                System.out.println("等待结束，被唤醒");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                lock.unlock();
            }
        }).start();

        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        new Thread(() -> {
            lock.lock();

            try {
                System.out.println("准备开始唤醒...");
                condition.signal();
                System.out.println("唤醒了，但还没有释放锁");
            } finally {
                lock.unlock();
                System.out.println("唤醒了，并且也已经释放锁了");
            }

        }).start();
    }
}
