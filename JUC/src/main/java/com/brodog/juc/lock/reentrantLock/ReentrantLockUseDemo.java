package com.brodog.juc.lock.reentrantLock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock 使用 demo
 * @author By-BroDog
 * @date 2024-05-19
 */
public class ReentrantLockUseDemo {

    private static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();
                doDeduction();
                lock.unlock();
            }
        }, "线程一").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();
                doDeduction();
                lock.unlock();
            }
        }, "线程二").start();
    }

    private static void doDeduction() {
        System.out.println(Thread.currentThread().getName() + "正在取钱");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "取完了");
    }
}
